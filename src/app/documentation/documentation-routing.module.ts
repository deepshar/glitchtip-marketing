import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentationPageComponent } from './documentation-page/documentation-page.component';
import { DocumentationIndexComponent } from './documentation-index/documentation-index.component';

const routes: Routes = [
  {
    path: '',
    component: DocumentationIndexComponent
  },
  {
    path: ':slug',
    component: DocumentationPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentationRoutingModule {}
