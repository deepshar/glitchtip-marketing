import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '../shared/shared.module';
import { QuestionAndAnswerComponent } from './question-and-answer/question-and-answer.component';
@NgModule({
  declarations: [PricingComponent, QuestionAndAnswerComponent],
  imports: [
    CommonModule,
    PricingRoutingModule,
    ScullyLibModule,
    MatCardModule,
    SharedModule,
  ],
})
export class PricingModule {}
