import { Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';
import { flattenedPlatforms } from 'glitchtip-frontend/src/app/settings/projects/platform-picker/platforms-for-picker';

import 'prismjs/prism.js';
import 'prismjs/components/prism-csharp.min.js';
import 'prismjs/components/prism-python.min.js';
import 'prismjs/components/prism-java.min.js';
import 'prismjs/components/prism-ruby.min.js';
import 'prismjs/components/prism-markup-templating.min.js';
import 'prismjs/components/prism-php.min.js';
import 'prismjs/components/prism-go.min.js';
import 'prismjs/components/prism-rust.min.js';

@Component({
  selector: 'app-sdk-docs',
  templateUrl: './sdk-docs.component.html',
  styleUrls: ['./sdk-docs.component.scss'],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class SDKDocsComponent {
  title$ = this.scully.getCurrent().pipe(
    map((currentRoute) => {
      const id = currentRoute.sourceFile?.slice(0, -3);
      return (
        flattenedPlatforms.find((platform) => platform.id === id)?.name || id
      );
    })
  );

  constructor(private scully: ScullyRoutesService) {}
}
